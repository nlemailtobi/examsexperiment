#!/bin/sh
env=`python3 -c 'import json, glob, os, re;
path = "/usr/share/nginx/html/main.*.js"
for filename in glob.glob(path):
    with open(filename, "r+") as f:
        text = f.read()
        text = re.sub("REST_API_URL_REPLACE", os.environ["REST_API_URL"], text)
        f.seek(0)
        f.write(text)
        f.truncate()'
`
ls -R
echo $env > /usr/share/nginx/html/assets/env.json
echo "daemon off;" >> /etc/nginx/nginx.conf
nginx