import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Exam} from './exam.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class ExamsApiService {

  private productUrl = "http://" + environment.REST_API_URL + ":5000";
  
  constructor(private http: HttpClient) {
  }

  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }

  // GET list of public, future events
  getExams(): Observable<Exam[]> {
    return this.http
      .get<Exam[]>(this.productUrl + `/exams`).pipe(
        
        catchError(this.handleError)
       );
  }

  handleError(error:HttpErrorResponse){
    console.log(error);
    return throwError(error.message);
 }    
}